define(['phaser'], function (Phaser) {
    'use strict';
    
    function Preloader (game) {
        // use init method!
    }
    
    Preloader.prototype.constructor = Preloader;
    
    Preloader.prototype.preload = function () {
	    // load images
	    // foreground
	    this.load.image('fg_courtain_left', 'assets/img/sprites/foreground/courtain_left.png');
	    this.load.image('fg_courtain_right', 'assets/img/sprites/foreground/courtain_right.png');
	    this.load.image('fg_courtain_top', 'assets/img/sprites/foreground/courtain_top.png');
	    // hud: in-game
	    this.load.image('hud_ingame', 'assets/img/sprites/hud/menu_ingame.png');
	    // buttons
	    this.load.image('btn_screen', 'assets/img/sprites/buttons/btn_screen.png');
	    // level 01 - The Stage
	    this.load.image('lvl01_bg', 'assets/img/levels/level01/background.png');
	    this.load.image('lvl01_ground', 'assets/img/levels/level01/ground.png');
	    this.load.image('lvl01_platform', 'assets/img/levels/level01/platform.png');

	    // load spritesheets
	    // player: eros
	    this.load.spritesheet('spr_eros_jump', 'assets/img/sprites/eros/jump.png', 67, 55);
	    this.load.spritesheet('spr_eros_stand', 'assets/img/sprites/eros/stand.png', 58, 73);
	    this.load.spritesheet('spr_eros_walk', 'assets/img/sprites/eros/walk.png', 60, 76);
	    // houses: house style 01
	    this.load.spritesheet('spr_house01', 'assets/img/sprites/house/house01.png', 220, 242);

	    // load audio
        // TODO: Load audio

	    // load json configuration files
	    this.load.json('character', 'assets/data/character.json');
    };
    
    Preloader.prototype.create = function () {
        this.game.state.start('Game');
    };
    
    return Preloader;
});