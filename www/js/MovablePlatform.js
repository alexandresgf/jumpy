define(['phaser', 'Platform', 'CustomEvents'], function (Phaser, Platform, CustomEvents) {
	'use strict';

	function MovablePlatform(game, sprite, direction, speed) {
		Platform.call(this, game, 0, 0, sprite);

		/**
		 * platform direction
		 * 0 = move from right to left
		 * 1 = move from left to right
		 */
		this._direction = direction;

		// platform speed
		this._speed = speed;

		// platform initial x position
		this._initX = 0;

		// platform initial y position
		this._initY = 0;

		// platform final x position
		this._finalX = 0;

		// platform final y position
		this._finalY = 0;
	}

	MovablePlatform.prototype = Object.create(Platform.prototype);
	MovablePlatform.prototype.constructor = MovablePlatform;

	MovablePlatform.prototype.update = function () {
		switch (this._direction) {
			// check negative bounds outside the screen
			case 0:
				if (this.x < this.finalPos.x) {
					this.stop();
					this.reset();
					CustomEvents.onPlatformDone.dispatch(this);
				}
				break;

			// check positive bounds outside the screen
			case 1:
				if (this.x > this.finalPos.x) {
					this.stop();
					this.reset();
					CustomEvents.onPlatformDone.dispatch(this);
				}
				break;
		}
	};

	MovablePlatform.prototype.move = function () {
		switch (this._direction) {
			// move to left
			case 0:
				this.body.velocity.x = -this._speed;
				break;

			// move to right
			case 1:
				this.body.velocity.x = this._speed;
				break;
		}
	};

	MovablePlatform.prototype.stop = function () {
		this.body.velocity.x = 0;
	};

	MovablePlatform.prototype.reset = function () {
		this.x = this._initX;
		this.y = this._initY;
	};

	MovablePlatform.prototype.createPath = function (initX, initY, finalX, finalY) {
		this._initX = initX;
		this._initY = initY;
		this._finalX = finalX;
		this._finalY = finalY;
		this.x = this._initX;
		this.y = this._initY;
	};

	Object.defineProperty(MovablePlatform.prototype, 'direction', {
		get: function () {
			return this._direction;
		},

		set: function (value) {
			this._direction = value;
		}
	});

	Object.defineProperty(MovablePlatform.prototype, 'speed', {
		get: function () {
			return this._speed;
		},

		set: function (value) {
			this._speed = value;
		}
	});

	Object.defineProperty(MovablePlatform.prototype, 'initPos', {
		get: function () {
			var initX = this._initX;
			var initY = this._initY;

			return { x: initX, y: initY };
		}
	});

	Object.defineProperty(MovablePlatform.prototype, 'finalPos', {
		get: function () {
			var finalX = this._finalX;
			var finalY = this._finalY;

			return { x: finalX, y: finalY };
		}
	});

	Object.defineProperty(MovablePlatform.prototype, 'ID', {
		get: function () {
			return this._id;
		}
	});

	return MovablePlatform;
});