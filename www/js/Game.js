define(['phaser', 'Ground', 'Scene', 'Player', 'Platform', 'Trail'], function (Phaser, Ground, Scene, Player, Platform, Trail) {
	'use strict';

	function Game (game) {
        // use init method!
    }

    Game.prototype.constructor = Game;

	Game.prototype.init = function () {
		// google analytics track game screen
		// window.analytics.trackView('Jumpy - Game Screen');
		// load characters
		this._character = this.game.cache.getJSON('character');
	};

    Game.prototype.create = function () {
	    // start arcade physics
	    this.game.physics.startSystem(Phaser.Physics.ARCADE);

	    // set gravity on y-axis
	    this.game.physics.arcade.gravity.y = 1200;

	    // add backscreen color
	    this.game.stage.backgroundColor = '#000';

	    // create ground
	    this._ground = new Ground(this.game, -78, this.game.height - 160, 606, 160, 'lvl01_ground');

	    // create platform's trails
	    this._trails = new Phaser.ArraySet();

	    for (var i = 0; i < 4; i++) {
		    if (i % 2 === 0) {
			    this._trails.add(new Trail(this.game, 600 - 115 * i, 'lvl01_platform', 4, 0, 100));
		    } else {
			    this._trails.add(new Trail(this.game, 600 - 115 * i, 'lvl01_platform', 4, 1, 100));
		    }
	    }

	    // create player
	    this._player = new Player(this.game, this.game.width / 2, this.game.height / 2, this._character.player);

	    // create in-game menu
	    this._hud = new Phaser.Image(this.game, 58, 89, 'hud_ingame');

	    // label style
	    var style = { font: 'bold 30px Arial', fill: '#000', boundsAlignH: 'center', boundsAlignV: 'middle' };
	    // create score label
	    this._txtScore = new Phaser.Text(this.game, this._hud.x + 18, this._hud.y + 40, '999999', style);
	    // create time label
	    this._txtTime = new Phaser.Text(this.game, this._txtScore.x + 132, this._txtScore.y, '00:00', style);

	    // create game controls
	    this.game.input.onTap.add(function (pointer, doubleTap) {
		    if (doubleTap) {
			    console.log('Double-Tap');
		    } else if (Math.floor(pointer.x / (this.game.width / 2)) === 0) {
			    console.log('TAP left');
			    this._player.walk(0);
		    } else {
			    console.log('TAP right');
			    this._player.walk(1);
		    }
	    }, this);

	    this.game.input.onHold.add(function () {
		    console.log('Hold');
	    }, this);

	    this.game.input.onUp.add(function () {
		    console.log('Up');
	    }, this);

	    // create scene
	    this._scene = new Scene(this.game);
	    // scene background
	    this._scene.addToBg(new Phaser.Image(this.game, 0, 0, 'lvl01_bg'));
	    this._scene.addToBg(this._ground);
	    // scene middleground
	    this._trails.list.forEach(function (trail) {
		    this._scene.addToMg(trail.platforms);
	    }, this);
	    this._scene.addToMg(this._player);
	    // scene foreground
	    this._scene.addToFg(new Phaser.Image(this.game, 0, 0, 'fg_courtain_left'));
	    this._scene.addToFg(new Phaser.Image(this.game, this.game.width - 79, 0, 'fg_courtain_right'));
	    this._scene.addToFg(this._hud);
	    this._scene.addToFg(this._txtScore);
	    this._scene.addToFg(this._txtTime);
	    this._scene.addToFg(new Phaser.Image(this.game, 0, 0, 'fg_courtain_top'));

	    // start the trails
	   this._trails.callAll('start');
    };

	Game.prototype.update = function () {
		// check player collision
		this.game.physics.arcade.collide(this._player, this._ground);
		this._trails.list.forEach(function (trail) {
			this.game.physics.arcade.collide(this._player, trail.platforms);
		}, this);
	};

	Game.prototype.gameOver = function () {
		// TODO: code me!
	};

	Game.prototype.save = function () {
		// TODO: code me!
	};

    return Game;
});