define(['phaser', 'Character'], function (Phaser, Character) {
	'use strict';

	function Player (game, x, y, character) {
		Character.call(this, game, x, y, character);

		// collide with world bounds
		this.body.collideWorldBounds = true;

		// prevent platform collisions
		this.body.checkCollision.up = false;
		this.body.checkCollision.left = false;
		this.body.checkCollision.right = false;

		// resize bound box
		this.body.setSize(this.width - 10, this.height - 10, 5, 10);

		// player score
		this._score = 0;

		// control flags
		this._isWalk = false;
		this._isJump = false;
		this._isMoveLeft = false;
		this._isMoveRight = false;
	}

	Player.prototype = Object.create(Character.prototype);
	Player.prototype.constructor = Player;

	Player.prototype.update = function () {
		if (this._isWalk && this._isMoveLeft) {
			this.body.velocity.y = -100;
			this.body.velocity.x = -this.speed.x;
			this.resetControlFlags();
			// this.loadTexture('spr_eros_walk');
			// var walk = this.animations.add('walk');
			// this.body.velocity.x = -this.speed.x;
			// walk.play();
			// walk.onComplete.add(function () {
			// 	self.resetControlFlags();
			// });
		} else if (this._isWalk && this._isMoveRight) {
			this.body.velocity.y = -100;
			this.body.velocity.x = this.speed.x;
			this.resetControlFlags();
		} else if (this._isJump && this._isMoveLeft) {
			this.body.velocity.y = -this.speed.y;
			this.body.velocity.x = -this.speed.x;
			this.resetControlFlags();
		} else if (this._isJump && this._isMoveRight) {
			this.body.velocity.y = -this.speed.y;
			this.body.velocity.x = this.speed.x;
			this.resetControlFlags();
		} else if (this.body.touching.down) {
			this.body.velocity.x = 0;
		}
	};

	Player.prototype.resetControlFlags = function () {
		this._isWalk = false;
		this._isJump = false;
		this._isMoveLeft = false;
		this._isMoveRight = false;
	};

	Player.prototype.walk = function (direction) {
		if (this.body.touching.down) {
			this._isWalk = true;

			switch (direction) {
				// walk left
				case 0:
					this._isMoveLeft = true;
					break;

				// walk right
				case 1:
					this._isMoveRight = true;
					break;
			}
		}
	};

	Player.prototype.jump = function (direction) {
		if (this.body.touching.down) {
			this._isJump = true;

			switch (direction) {
				// jump left
				case 0:
					this._isMoveLeft = true;
					break;

				// jump right
				case 1:
					this._isMoveRight = true;
					break;
			}
		}
	};

	Player.prototype.addScore = function (value) {
		this._score += value;
	};

	Object.defineProperty(Player.prototype, 'score', {
		get: function () {
			return this._score;
		},

		set: function (value) {
			this._score = value;
		}
	});

	return Player;
});
