define(['phaser'], function (Phaser) {
	'use strict';

	function Scene(game) {
		// game handler
		this.game = game;
		// background group
		this._bg = this.game.add.group();
		// middleground group
		this._mg = this.game.add.group();
		// foreground group
		this._fg = this.game.add.group();
	}

	Scene.prototype.constructor = Scene;

	Scene.prototype.addToBg = function (entity) {
		this._bg.add(entity);
	};

	Scene.prototype.addToMg = function (entity) {
		this._mg.add(entity);
	};

	Scene.prototype.addToFg = function (entity) {
		this._fg.add(entity);
	};

	Scene.prototype.removeAll = function () {
		this._bg.removeAll(true);
		this._mg.removeAll(true);
		this._fg.removeAll(true);
	};

	return Scene;
});
