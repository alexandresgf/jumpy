define(['phaser', 'Entity'], function (Phaser, Entity) {
	'use strict';

	function Platform(game, x, y, sprite) {
		Entity.call(this, game, x, y, sprite);

		// set the anchor to origin
		this.anchor.set(0);

		// disable gravity
		this.body.allowGravity = false;

		// turn it a rigid body
		this.body.immovable = true;
	}

	Platform.prototype = Object.create(Entity.prototype);
	Platform.prototype.constructor = Platform;

	return Platform;
});