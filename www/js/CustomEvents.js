define(['phaser'], function (Phaser) {
	'use strict';

	function CustomEvents() {
		// movable platform reach final position
		this._onPlatformDone = new Phaser.Signal();
	}

	CustomEvents.prototype.constructor = CustomEvents;

	Object.defineProperty(CustomEvents.prototype, 'onPlatformDone', {
		get: function () {
			return this._onPlatformDone;
		}
	});

	var customEvents = new CustomEvents();

	return {
		onPlatformDone: customEvents.onPlatformDone
	};
});