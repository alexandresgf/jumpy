define(['phaser', 'MovablePlatform', 'CustomEvents'], function (Phaser, MovablePlatform, CustomEvents) {
	'use strict';

	const PLATFORM_GAP = 10;

	function Trail(game, posY, sprite, total, direction, speed) {
		// game handler
		this.game = game;

		// total of platforms
		this._total = total;

		// trail group of platforms
		this._trail = this.game.add.group();

		// fill the group with platforms
		for (var i = 0; i < total; i++) {
			var platform = new MovablePlatform(this.game, sprite, direction, speed);
			var initX;
			var finalX;

			switch (direction) {
				// move to left
				case 0:
					if (i === 0) {
						initX = this.game.width;
						finalX = - ((platform.width + PLATFORM_GAP) * (total - 1) + platform.width);
					} else {
						var lastPlatform = this._trail.getAt(i - 1);
						initX = lastPlatform.right + PLATFORM_GAP;
						finalX = lastPlatform.finalPos.x + lastPlatform.width + PLATFORM_GAP - 2;
					}
					break;

				// move to right
				case 1:
					if (i === 0) {
						initX = - platform.width;
						finalX = this.game.width + (platform.width + PLATFORM_GAP) * (total - 1);
					} else {
						var lastPlatform = this._trail.getAt(i - 1);
						initX = lastPlatform.initPos.x - lastPlatform.width - PLATFORM_GAP;
						finalX = lastPlatform.finalPos.x - lastPlatform.width - PLATFORM_GAP + 2;
					}
					break;
			}

			platform.createPath(initX, posY, finalX, posY);
			this._trail.add(platform);
		}

		// customized event to check if the platform has finished the path
		CustomEvents.onPlatformDone.add(function (platform) {
			try {
				if (this._trail.getChildIndex(platform) === total - 1) {
					this._trail.callAll('move');
				}
			} catch (e) {
				// do nothing!
			}
		}, this);
	}

	Trail.prototype.constructor = Trail;

	Trail.prototype.start = function () {
		this._trail.callAll('move');
	};

	Object.defineProperty(Trail.prototype, 'platforms', {
		get: function () {
			return this._trail;
		}
	});

	return Trail;
});