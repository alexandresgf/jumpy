define(['phaser', 'Entity'], function (Phaser, Entity) {
	'use strict';

	function Character(game, x, y, character) {
		Entity.call(this, game, x, y, character.sprite);

		// character name
		this.name = character.name;

		// maximum value of health
		this.maxHealth = character.hp;

		// health points
		this.health = this.maxHealth;

		// character speed
		this._speed = character.speed;

		// character jump strength
		this._jump = character.jump;
	}

	Character.prototype = Object.create(Entity.prototype);
	Character.prototype.constructor = Character;

	Object.defineProperty(Character.prototype, 'speed', {
		get: function () {
			return this._speed;
		}
	});

	return Character;
});
